
#####################################
## python Extract.py filename.pos startFrame endFrame outputFile.pos

############

#!/nishome/fmbahchrameh/miniconda3/bin/python
import sys
import linecache
import numpy as np
import glob


# prepare hoomd run
inputFile  = sys.argv[1]
startFrame = sys.argv[2]
endFrame   = sys.argv[3]
outFile = sys.argv[4]
f = open(inputFile, 'r')

# pre-analyze the frames
entry = "eof"
countFrames = 0
countLine = 0
frameData = []
frameData.append([0, 0])
for line in f:
    countLine = countLine + 1
    if entry  in line:
        countFrames = countFrames + 1
        frameData.append([countFrames, countLine])
f.close()


def ExtractFrame(i):
   # f = open(inputFile, 'r')

    frameNumber    = int(frameData[i][0])
    lineNumber     = int(frameData[i][1])
    nextLineNumber = int(frameData[i + 1][1])
   
    # get meta data that is pressure, density etc
    frameInfor       = []
    #stores title for pressure, density etc
    frameInfor.append(linecache.getline(inputFile, lineNumber + 1) )
    # add energy 

    #stores values for pressure, density etc
    frameInfor.append(linecache.getline(inputFile, lineNumber + 2))

    # get box length
    box = []
    box.append(linecache.getline(inputFile, lineNumber + 4))
    boxSize = float(box[0].split()[1])

    particleNumber = 0
    array = []

    for line_number in range(lineNumber + 6, nextLineNumber):
        getline = linecache.getline(inputFile, line_number)
        #print(line_number)
        x = getline.split()
        array.append([float(x[0]), float(x[1]), float(x[2])])
        particleNumber = particleNumber + 1

    return (boxSize, particleNumber, frameInfor, array)


def main(start, end):
    posFile = open(outFile, 'w')

    for frame in range(start, end):
        box, N, frameInfor, P = ExtractFrame(frame)
       # print(P)
        splitString       = frameInfor[0].split()
        splitStringValues = frameInfor[1].split()
        P = np.array(P)
        #print("split = ", splitString)
        
        newHeader = " "
        newHeaderValues = " "
        for entry in splitString:
            newHeader = newHeader + " " + entry

        for values in splitStringValues:
            newHeaderValues = newHeaderValues + " " + str(values)
        
       # print("header infor = ", newHeader )
        posFile.write(newHeader + "\n")
        posFile.write(newHeaderValues + "\n")
        posFile.write("#[done]" + "\n")
        posFile.write("box" + " " +  str(box) + " " + str(box) + " " + str(box) + "\n")
        posFile.write("shape 'sphere 1 ffff0000' " + "\n")
        for i in range(N):
           
            posFile.write(str(P[i, 0]) + " " + str(P[i, 1]) + " " + str(P[i, 2]) + "\n")

        posFile.write("eof" + "\n")
    posFile.close()


main(int(startFrame), int(endFrame))






