#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <strings.h>
#include <algorithm>
#include <map>
#include <utility>
#include <iterator>


#define NOISE           -2
#define UNCLASSIFIED    -1

// parameter for spherical harmonics
static int LTOTAL = 6;

// DBSCAN parameters
static double EPSILON = 1.4;//1.40;
static int MINPTS = 4;
static double CUTOFF = 0.90;//0.93;

struct Atom
{
	Atom()
	{
		radius = 0.5;
		color = 0xffffff;
	}
	
	// particle data
	Vector3d position;
	double radius;
	int color;
	int index;
	
	// bond order data
	std::vector<double> qlm;
	// neighbors
	std::vector<int> neighbors;
	// cluster id
	int cluster_id;

	void print(double boxLength)
	{
		double boxHalf = boxLength * 0.5;
		std::cout << "sphere " << (2.0 * radius) << " " << std::hex << color << std::dec;
		std::cout << " " << (position.x - boxHalf);
		std::cout << " " << (position.y - boxHalf);
		std::cout << " " << (position.z - boxHalf);
		std::cout << "\n";
	}
    void writeFile(std::ofstream& file, double boxLength)
    {
        double boxHalf = boxLength * 0.5;
        file << "sphere " << (2.0 * radius) << " " << std::hex << color << std::dec;
        file << " " << (position.x - boxHalf);
        file << " " << (position.y - boxHalf);
        file << " " << (position.z - boxHalf);
        //file << " " << index;
        file << "\n";
    }

};



class Frame
{


	public:

	Frame(const std::vector<std::string>& frameLines)
	{
		// prepare data structure
		atomList  = std::vector<Atom>();
		grainList  = std::vector<Grain>();
		//std::cout <<"Reading"<<std::endl;

		// read data from 'file'
		double newBoxLength = 0.0;
		char   cmd[999];
		bool   dataStarted = false;
		
		for (const std::string& line: frameLines)
		{
			std::stringstream iss(line);
			iss >> cmd;
				
			// done
			if (strcasecmp(cmd, "eof") == 0) break;
			
			//read the box
			else if (strcasecmp(cmd, "box") == 0)
			{
				iss >> newBoxLength;
				iss >> cmd;
				iss >> cmd;
			}
				
			// data starts
			else if (strcasecmp(cmd, "shape") == 0)
			{
				dataStarted = true;
				iss >> cmd;
				// skip radius and color
				iss >> cmd;
				iss >> cmd;
			}
			
			// read particle
			else if (strcasecmp(cmd, "sphere") == 0 || dataStarted)
			{
				if (strcasecmp(cmd, "sphere") == 0)
				{
					// skip radius and color
					iss >> cmd;
					iss >> cmd;
					// prepare to read particle
					iss >> cmd;
				}
				
				// read new particle
				Atom atom = Atom();
				double x, y, z;
				x = atof(cmd);
				iss >> y;
				iss >> z;
				boxLength = newBoxLength;
				double boxHalf = newBoxLength * 0.5;
				atom.position = Vector3d(x + boxHalf, y + boxHalf, z + boxHalf);
				atomList.push_back(atom);
				
			}
		}
	}
};

class processFile
{
    public:
	// sets the frame data
		processFile(std::istream& file)
		{
			// complete simulation data
			std::vector<std::vector<std::string> > fileLines;

			// parse frames
			int frameNumber = 0;
			while (!file.eof())
			{
				//std::cout <<"reading in file" << std::endl;
				std::string line;
				getline(file, line);
				
				// new std::vector for storage, if necessary
				if (fileLines.size() == frameNumber) fileLines.push_back(std::vector<std::string>());
				fileLines[frameNumber].push_back(line);
				if (line.find("eof") != std::string::npos)
				{
					// frame is finished
					frameNumber++;
					continue;
				}
				if (frameNumber >= 4000) break;
			}
			
			// initialize frames
			for (const std::vector<std::string>& frameLines : fileLines)
			{
				frames.push_back(Frame(frameLines));
			}
			//set globalFileLines
			int rows = 0; 
			for (auto& frameLines : fileLines)
			{
				if (globalFileLines.size() == rows) globalFileLines.push_back(std::vector<std::string>());
				for (auto& line : frameLines)
				{
					globalFileLines[rows].push_back(line);
				}
				rows++;

			}
		}
};
	